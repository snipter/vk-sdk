var request = require("request");
var url = require("url");
var async = require("async");
var _ = require("underscore");
var fs = require("fs");
var rest = require('restler');
var temp = require("temp").track();

/*========= Access token =========*/

var vkAccessToken;

var setAccessToken = function(at){
	vkAccessToken = at;
}

var getAccessToken = function(at){
	return at;
}

/*============ Auth ============*/

function authServerAccessToken(clientID, clientSecret, callback){
	var opt = {};
	opt.url = 'https://oauth.vk.com/access_token';
	opt.qs = {
		client_id: clientID,
		client_secret: clientSecret,
		grant_type: 'client_credentials',
		v: '5.27'
	};
	request(opt, function(err, res, body){
		if(err) return callback(err);
		if(!body) return callback("body not set");
		var data = body;
		try{
			if(typeOf(data) == "string") data = JSON.parse(data);
		}catch(err){
			callback("data parsing error: " + err);
		}
		if(data.error) return callback(data.error);
		var token = data.access_token;
		if(!token) return callback("token not set");
		callback(null, token);
	})
}

/*========= Users =========*/

var usersGet = function(uid, options, callback){
	var method = "users.get";
	if(!options) options = {};
	options.user_ids = uid;
	requestWithMethod(method, options, callback);
}

var usersGetSubscriptions = function(uid, options, callback){
	var method = "users.getSubscriptions";
	if(!options) options = {};
	options.user_id = uid;
	requestWithMethod(method, options, callback);
}

var usersGetFollowers = function(uid, options, callback){
	var method = "users.getFollowers";
	if(!options) options = {};
	options.user_id = uid;
	requestWithMethod(method, options, callback);
}

/*========= Friends =========*/

//* Response:
//* [uid, uid, uid, .... uid]
var friendsGet = function(uid, options,callback){
	var method = "friends.get";
	if(!options) options = {};
	options.user_id = uid;
	var req = composeAPIRequest(method, options);
	request(req, function(err, res, body){
		if(err) return callback(err);
		if(res.statusCode != 200) return callback("server status code: " + res.statusCode);
		var data = JSON.parse(body);
		if(!data) return callback("data parsing error");
		if(data.error) return callback(data.error);
		if(!data.response) return callback("response not set");
		callback(null, data.response);
	})
}

var friendsGetRequests = function(options, callback){
	var method = "friends.getRequests";
	if(!options) options = {};
	requestWithMethod(method, options, callback);
}

var friendsGetOutputRequests = function(options, callback){
	var method = "friends.getRequests";
	if(!options) options = {};
	options.out = 1;
	requestWithMethod(method, options, callback);
}

var friendsGetAllOutputRequests = function(options, callback){
	var count = 1000;
	var offset = 0;
	var uids = [];
	var lastRequestUIDSCount = count;

	if(!options) options = {};
	options.count = count;

	async.whilst(
		function(){return lastRequestUIDSCount == count},
		function(callback){
			options.offset = offset;
			friendsGetOutputRequests({offset: offset, count: count}, function(err, newUids){
				if(err) return callback(err);
				uids = uids.concat(newUids);
				lastRequestUIDSCount = newUids.length;
				offset += count
				callback();
			})
		},
		function(err){
			if(err) return callback(err);
			callback(null, uids);
		}
	);
}

/*========= Groups =========*/

var groupsGetAllMembers = function(group_id, options, progress, callback){
	var users = [];
	var lastRequest = false;
	var iteration = 0;
	var lastProcent = 0;
	async.whilst(
		function(){return !lastRequest},
		function(callback){
			options.count = 1000;
			options.offset = (iteration++ * 1000);
			groupsGetMembers(group_id, options, function(err, data){
				if(err) return callback(err);
				users = users.concat(data.users);
				if(data.users.length < 1000) lastRequest = true;
				if(progress){
					var processed = iteration * 1000;
					if(processed > data.count) processed = data.count;
					var procent  = Math.floor((processed / data.count) * 100);
					if(procent > lastProcent){
						lastProcent = procent;
						progress(procent);
					}
				} 
				callback();
			});
		}, function(err){
			if(err) return callback(err);
			callback(null, users);
		}
	);
}

var groupsGetMembers = function(group_id, options, callback){
	var method = "groups.getMembers";
	if(!options) options = {};
	options.group_id = group_id;
	var req = composeAPIRequest(method, options);
	request(req, function(err, res, body){
		if(err) return callback(err);
		if(res.statusCode != 200) return callback("server status code: " + res.statusCode);
		var data = JSON.parse(body);
		if(!data) return callback("data parsing error");
		if(data.error) return callback(data.error);
		if(!data.response) return callback("response not set");
		callback(null, data.response);
	})
}

/*========= Photos =========*/

var photosGetAlbums = function(oid, options, callback){
	var method = "photos.getAlbums";
	if(!options) options = {};
	options.owner_id = oid;
	requestWithMethod(method, options, callback);
}

var photosGet = function(oid, aid, options, callback){
	var method = "photos.get";
	if(!options) options = {};
	options.owner_id = oid;
	options.album_id = aid;
	requestWithMethod(method, options, callback);
}

var photosUploadPhotoFromURLToWall = function(uri, options, callback){
	downloadFileToTempFolder(uri, function(err, fileName){
		if(err) return callback(err);
		photosUploadPhotoFromFileToWall(fileName, options, function(err, uploadData){
			if(err) return callback(err);
			fs.unlink(fileName, function(err){
				if(err) return callback(err);
				callback(null, uploadData);
			});
		});
	});
}

var downloadFileToTempFolder = function(uri, callback){
	request.head(uri, function(err, res, body){
		var contentType = res.headers['content-type'];
		var contentLength = res.headers['content-length'];
		var contentExt = "." + contentTypeToExt(contentType);
		temp.open({prefix: "vk-sdk", suffix: contentExt}, function(err, fileInfo){
			if(err) return callback(err);
			request(uri).pipe(fs.createWriteStream(fileInfo.path)).on('close', function(err){
				if(err) return callback(err);
				callback(null, fileInfo.path);
			});
		});
	});
}

function contentTypeToExt(contentType){
	contentType = contentType.toLowerCase();

	//images
	if(contentType.indexOf("image/gif")>=0) return "gif";
	if(contentType.indexOf("image/jpeg")>=0) return "jpg";
	if(contentType.indexOf("image/png")>=0) return "png";
	if(contentType.indexOf("image/pjpeg")>=0) return "jpg";
	if(contentType.indexOf("image/svg+xml")>=0) return "jpg";

	return "";
}


// Required parameters:
// - group_id (без знака минус)
//
// Returned parameters:
//
var photosUploadPhotoFromFileToWall = function(fileName, options, callback){
	fs.exists(fileName, function(fileExists){
		if(!fileExists) return callback("photo file not found");
		photosGetWallUploadServer(options, function(err, uploadData){
			if(err) return callback(err);
			uploadPhotoToLink(fileName, uploadData.upload_url, function(err, uploadFileData){
				if(err) return callback(err);
				options.photo = uploadFileData.photo;
				options.server = uploadFileData.server;
				options.hash = uploadFileData.hash;
				photosSaveWallPhoto(options, function(err, uploadData){
					if(err) return callback(err);
					callback(null, uploadData);
				});
			});
		});
	});
}

// Required parameters:
// - group_id (без знака минус)
// 
// Returned parameters:
// - upload_url
// - album_id
// - user_id
//
var photosGetWallUploadServer = function(options, callback){
	var method = "photos.getWallUploadServer";
	requestWithMethod(method, options, callback);
}

// Required parameters:
// - user_id | group_id (без знака минус)
// - photo - возвращается сервером после загрузки
// - server - возвращается сервером после загрузки
// - hash - возвращается сервером после загрузки
// 
// Returned parameters:
// - id
// - pid - photo id
// - aid
// - owner_id - 
// - src - 
// - src_big
// - src_small
// - src_xbig - если фотка достаточного размера, то будет и этот параметр
// - src_xxbig - если фотка достаточного размера, то будет и этот параметр
// - created
//
var photosSaveWallPhoto = function(options, callback){
	var method = "photos.saveWallPhoto";
	requestWithMethod(method, options, callback);
}

var uploadPhotoToLink = function(fileName, url, callback){
	var stats = fs.statSync(fileName);
	var contentLength = stats["size"];
	rest.post(url, {
		multipart: true,
		data: {'photo': rest.file(fileName, null, contentLength, null, 'image/jpeg')}
	}).on('complete', function(result){
		if (result instanceof Error) return callback(result.message);
		try{
			var data = JSON.parse(result);
			return callback(null, data);
		}catch(e){
			return callback("data parsing error");
		}
	});
}


/*========= Wall =========*/

var wallPost = function(options, callback){
	var method = "wall.post";
	if(!options) options = {};
	if(!options.attachments) return requestWithMethod(method, options, callback);

	var group_id = null;
	if(options.owner_id) group_id = options.owner_id.replace("-", "");

	var attachments = formatAttachments(options.attachments);
	if(!attachments) return callback("wrong attachments format");

	uploadAttachments(attachments, options, function(err, attachmentsStr){
		if(err) return callback(err);
		options.attachments = attachmentsStr;
		requestWithMethod(method, options, callback);
	})
}

var wallRepost = function(options, callback){
	var method = "wall.repost";
	if(!options) options = {};
	requestWithMethod(method, options, callback);
}

var wallRepostWallRecord = function(oid, pid, options, callback){
	if(!options) options = {};
	var postID = "wall" + oid + "_" + pid;
	options.object = postID;
	wallRepost(options, callback);
}


function wallGetAllComments(options, callback){
	var count = 100;
	var offset = 0;
	var comments = [];
	var lastRequestCount = count;

	if(!options) options = {};
	options.count = count;

	async.whilst(
		function(){return lastRequestCount == count},
		function(callback){
			options.offset = offset;
			wallGetComments(options, function(err, data){
				if(err) return callback(err);
				data.splice(0, 1);
				comments = comments.concat(data);
				lastRequestCount = data.length;
				offset += count
				callback();
			})
		},
		function(err){
			if(err) return callback(err);
			callback(null, comments);
		}
	);
}

function wallGetComments(options, callback){
	var method = "wall.getComments";
	if(!options) options = {};
	requestWithMethod(method, options, callback);
}





/*========= Attachments =========*/

var formatAttachments = function(att){
	if(att == null) return null; 
	if(typeOf(att) == "string") return [att];
	if(typeOf(att) == "array") return att;
	return att;
}

var uploadAttachments = function(attachments, options, callback){
	var attArr = [];
	async.each(attachments, function(attachment, callback){
		uploadAttachment(attachment, options, function(err, attStr){
			if(err) return callback(err);
			attArr.push(attStr);
			callback();
		});
	}, function(err){
		if(err) return callback(err);
		var attStr = composeAttachmentStr(attArr);
		callback(null, attStr);
	});
}

var uploadAttachment = function(attStr, options, callback){
	if(isVkMediaLink(attStr)) return callback(null, attStr);
	if(isYoutubeLink(attStr)) return uploadYoutubeVideo(attStr, options, callback);
	if(isWebLink(attStr)){
		urlContentType(attStr, function(err, contentType){
			if(isContentTypeImage(contentType)){
				options = cloneObj(options);
				options.group_id = removeMinus(options.owner_id);
				delete options.owner_id;
				return photosUploadPhotoFromURLToWall(attStr, options, function(err, photoArr){
					if(err) return callback(err);
					var photo = photoArr[0];
					callback(null, photo.id);
				});
			}
			if(isContentTypeText(contentType)) return callback(null, attStr);
			return callback("unknow attachment format");
		});
	}else return callback("unknow attachment format");
}

var uploadYoutubeVideo = function(link, options, callback){
	if(options.owner_id) {
		options = cloneObj(options);
		options.group_id = removeMinus(options.owner_id); 
		delete options.owner_id;
	}
	videoSaveYoutube(link, options, function(err, videoData){
		if(err) return callback(err);
		var attStr = videoDataToStr(videoData);
		callback(null, attStr);
	});
}

var removeMinus = function(str){
	return str.replace("-", "");
}

var composeAttachmentStr = function(arr){
	var str;
	_.each(arr, function(item, index){
		if(index == 0) str = item;
		else str += "," + item;
	})
	return str;
}

var videoDataToStr = function(data){
	return "video"+data.oid+"_"+data.vid;
}

var isYoutubeLink = function(url){
	if(extractYoutubeVideoID(url)) return true;
	else return false;
}

var isVkMediaLink = function(url){
	var regexp = /[A-z]+[\d-]+_\d+/g;
	if(regexp.test(url)) return true;
	else return false;
}

var isWebLink = function(url){
	var regexp = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
	if(regexp.test(url)) return true;
	else return false;
}

var urlContentType = function(url, callback){
	request.head(url, function(err, res, body){
		if(err) return callback(err);
		callback(null, res.headers['content-type']);
	});
}

var isContentTypeImage = function(contentType){
	contentType = contentType.toLowerCase();
	if(contentType.indexOf("image/gif") >= 0) return true;
	if(contentType.indexOf("image/jpeg") >= 0) return true;
	if(contentType.indexOf("image/pjpeg") >= 0) return true;
	if(contentType.indexOf("image/png") >= 0) return true;
	if(contentType.indexOf("image/svg+xml") >= 0) return true;
	return false;
}

var isContentTypeText = function(contentType){
	contentType = contentType.toLowerCase();
	if(contentType.indexOf("text/html") >= 0) return true;
	return false;
}

/*========= Video =========*/

var videoSave = function(options, callback){
	var method = "video.save";
	if(!options) options = {};
	requestWithMethod(method, options, callback);
}

var videoSaveYoutube = function(link, options, callback){
	if(!options) options = {};
	options.link = link;
	videoSave(options, function(err, data){
		if(err) return callback(err);
		var uploadURL = data.upload_url;
		var vid = data.vid;
		var oid = data.owner_id;
		videoSaveYoutubeRequest(uploadURL, function(err){
			if(err) return callback(err);
			callback(null, {vid: vid, oid: oid});
		})
	})
}

var videoSaveYoutubeRequest = function(uploadURL, callback){
	request.post({
	  url: uploadURL
	}, function(err, response, body){
	  if(err) return callback(err);
	  if(!body) return callback("Save youtube video request: body is empty");
	  body = JSON.parse(body);
	  if(!body.response) return callback("Save youtube video request: response code not set");
	  if(!(body.response == 1)) return callback("Save youtube video request: wrong body response code");
	  callback(null);
	});
}

/*========= Likes =========*/

var likesAdd = function(options, callback){
	var method = "likes.add";
	if(!options) options = {};
	requestWithMethod(method, options, callback);
}

var likesGetList = function(options, callback){
	var method = "likes.getList";
	if(!options) options = {};
	requestWithMethod(method, options, callback);
}

var likesIsLiked = function(options, callback){
    var method = "likes.isLiked";
    if(!options) options = {};
    requestWithMethod(method, options, callback);
}

/*========= Polls =========*/

//
//	Get poll id from wall post
//	https://api.vk.com/method/wall.getById?posts=221126708_995
//
var pollsGetById = function(opt, callback){
	var method = "polls.getById";
	requestWithMethod(method, opt, callback);
}

/*============ Messages ============*/

var messagesSend = function(opt, callback){
	var method = "messages.send";
	requestWithMethod(method, opt, callback);
}

/*============ Secure ============*/

var secureCheckToken = function(opt, callback){
	var method = "secure.checkToken";
	requestWithMethod(method, opt, callback);
}

/*============ Auth link ============*/

function clientAuthLink(app_id, scope){
	if(!app_id) return null;
	var link = "https://oauth.vk.com/authorize";
	var data = {};
	data.client_id = app_id;
	data.redirect_uri = "https://oauth.vk.com/blank.html";
	data.display = "page";
	data.v = "5.20";
	data.response_type = "token";
	if(scope) data.scope = scope;
	return composeURL(link, data);
}

function extractAccessTokenFromLink(url){
	var regex = /([a-z_]+)=([a-z0-9]+)/g;
	var data = regex.execAll(url);
	var res = {};
	_.each(data, function(item){
		res[item[1]] = item[2];
	})
	return res;
}

function composeURL(link, data){
	var keys = Object.keys(data);
	for(var i = 0; i < keys.length; i++){
		var el = keys[i] + "=" + encodeURIComponent(data[keys[i]]);
		if(i == 0) link += "?" + el;
		else link += "&" + el;
	}
	return link;
}

/*========= Functions =========*/

var requestWithMethod = function(method, options, callback){
	//Setting access tokent
	if(!options) options = {};
	if(options.at) options.access_token =  options.at;
	if(!options.access_token && vkAccessToken) options.access_token =  vkAccessToken;
	
	//Make request
	var req = composeAPIRequest(method, options);
	request(req, function(err, res, body){
		if(err) return callback(err);
		if(res.statusCode != 200) return callback("server status code: " + res.statusCode);
		var data = JSON.parse(body);
		if(data.error) return callback(data.error);
		if((data.response === null) || (data.response === undefined) ) return callback("response not set");
		callback(null, data.response);
	})
}

var composeAPIRequest = function(method, data){
	var u = {};
	u.protocol = "https";
	u.host = "api.vk.com/method/" + method;
	u.query = convertOptions(data);
	return url.format(u);
}

var convertOptions = function(options){
	var keys = Object.keys(options);
	_.each(keys, function(key){
		if(isArray(options[key])) options[key] = arrayToStr(options[key]);
	});
	return options;
}

var arrayToStr = function(arr){
	var str = "";
	_.each(arr, function(item){
		if(str == "") str += item;
		else str += "," + item;
	});
	return str;
}

var isArray = function(obj){
	return Object.prototype.toString.call( obj ) === '[object Array]';
}

function extractYoutubeVideoID(link){
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (link.match(p)) ? RegExp.$1 : false;
}

function typeOf(value) {
    var s = typeof value;
    if (s === 'object') {
        if (value) {
            if (Object.prototype.toString.call(value) == '[object Array]') s = 'array';
        } else {
            s = 'null';
        }
    }
    return s;
}

function checkTypes(argList, typeList) {
    for (var i = 0; i < typeList.length; i++) {
        if (typeOf(argList[i]) !== typeList[i]) return false;
    }
    return true;
}

function cloneObj(obj) {
    if(obj == null || typeof(obj) != 'object') return obj;
    var temp = obj.constructor(); // changed
    for(var key in obj) {
        if(obj.hasOwnProperty(key)) {
            temp[key] = cloneObj(obj[key]);
        }
    }
    return temp;
}

/*========= Exports =========*/

module.exports = {
	getAccessToken: getAccessToken,
	setAccessToken: setAccessToken,
	clientAuthLink: clientAuthLink,
	extractAccessTokenFromLink: extractAccessTokenFromLink,

	accessToken:{
		get: getAccessToken,
		set: setAccessToken
	},

	auth:{
		serverAccessToken: authServerAccessToken
	},

	users:{
		get: usersGet,
		getSubscriptions: usersGetSubscriptions,
		getFollowers: usersGetFollowers
	},

	friends:{
		get: friendsGet,
		getRequests: friendsGetRequests,
		getOutputRequests: friendsGetOutputRequests,
		getAllOutputRequests: friendsGetAllOutputRequests
	},

	groups: {
		getMembers: groupsGetMembers,
		getAllMembers: groupsGetAllMembers
	},

	photos: {
		get: photosGet,
		getAlbums: photosGetAlbums,
		getWallUploadServer: photosGetWallUploadServer,
		saveWallPhoto: photosSaveWallPhoto,
		uploadPhotoFromFileToWall: photosUploadPhotoFromFileToWall,
		uploadPhotoFromURLToWall: photosUploadPhotoFromURLToWall
	},

	wall: {
		post: wallPost,
		repost: wallRepost,
		repostWallRecord: wallRepostWallRecord,
		getComments: wallGetComments,
		getAllComments: wallGetAllComments
	},

	video:{
		save: videoSave,
		saveYoutube: videoSaveYoutube
	},

	likes: {
		add: likesAdd,
		getList: likesGetList,
		isLiked: likesIsLiked
	},
	
	polls: {
		getById: pollsGetById
	},

	messages: {
		send: messagesSend
	},

	secure: {
		checkToken: secureCheckToken
	}
}